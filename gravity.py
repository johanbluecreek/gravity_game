#!/usr/bin/env python3

import pygame, random, time
import numpy as np
from scipy.integrate import solve_ivp
from itertools import product

# globally set floats for np
#np_float = np.longdouble
np_float = np.float64

class Colors():

    def __init__(self):
        self.white = (255,255,255)
        self.black = (0,0,0)
        self.gray = (128,128,128)
        self.dark_gray = (70,70,70)
        self.green = (0,200,0)
        self.red = (200,0,0)
        self.blue = (50,50,230)
        self.yellow = (255,255,0)

class BackgroundStar():

    def __init__(self, position, absolute_magnitude):
        self.position = np.array(position, dtype=np_float)
        self.absolute_magnitude = random.normalvariate(absolute_magnitude, absolute_magnitude/1000)

        self.apparent_magnitude = absolute_magnitude
        self.projected_position = None

        diff = (absolute_magnitude - self.absolute_magnitude)*20000/absolute_magnitude
        yellow_shift = 25
        yellow = np.array([255,255,255-yellow_shift])
        if diff > 0:
            # smaller, more red
            self.color = yellow - np.array((0,min(abs(diff)*2,255),min(abs(diff)*2,255-yellow_shift)))
        else:
            # larger, more blue
            self.color = yellow - np.array((min(abs(diff),255),min(abs(diff),255),-min(abs(diff),yellow_shift)))

        self.draw = False

        self.not_in_window = False
        self.higher_dim_hidden = False
        self.wrong_size = False
        self.too_close = False

    def compute_apparent_magnitude(self, dim, ldf):
        self.apparent_magnitude = ldf*self.absolute_magnitude/np.linalg.norm(self.position)**(dim-1)

    def compute_projected_position(self, width, height, extra_dimension_size):
        self.not_in_window = False
        self.higher_dim_hidden = False
        self.wrong_size = False
        self.too_close = False
        min_magnitude = 1
        max_magnitude = 5
        min_distance = 350
        r_hat = self.position/np.linalg.norm(self.position)
        u = min_distance/r_hat[0]
        if len(r_hat) > 3:
            if not all([ self.position[i] + self.absolute_magnitude > extra_dimension_size or self.position[i] - self.absolute_magnitude < 0 for i in range(3,len(self.position))]):
                self.projected_position = np.array([-1000,-1000], dtype=np_float)
                self.draw = False
                self.higher_dim_hidden = True
                return None
            else:
                distance = np.array([ min(self.position[i], extra_dimension_size - self.position[i]) for i in range(3,len(self.position))])
                scale = np.linalg.norm(distance)
                fit = min_magnitude < self.apparent_magnitude < max_magnitude
                if self.apparent_magnitude > 1:
                    pass
                self.apparent_magnitude = self.apparent_magnitude*(1-scale)
                if fit and not min_magnitude < self.apparent_magnitude < max_magnitude:
                    self.higher_dim_hidden = True
        if u < min_distance:
            self.projected_position = np.array([-1000,-1000], dtype=np_float)
            self.draw = False
            self.too_close = True
            return None
        projected_position = np.array([u*r_hat[1], u*r_hat[2]], dtype=np_float)
        if not (-width/2 - self.apparent_magnitude < projected_position[0] < width/2 + self.apparent_magnitude and -height/2 - self.apparent_magnitude < projected_position[1] < height/2 + self.apparent_magnitude):
            self.projected_position = np.array([-1000,-1000], dtype=np_float)
            self.draw = False
            self.not_in_window = True
            return None
        self.projected_position = projected_position + np.array([width/2,height/2], dtype=np_float)
        if min_magnitude < self.apparent_magnitude < max_magnitude:
            self.draw = True
        else:
            self.wrong_size = True
            self.draw = False

class Background():

    def __init__(self, width, height, physics=None, number=300, box_size=120000):

        self.width = width
        self.height = height

        if physics is None:
            physics = Physics()

        self.dimensions = int(np.ceil(physics.dimensions))
        self.number = number
        self.box_size = box_size
        self.ldf = physics.ldf

        self.colors = Colors()

        self.background = self.colors.black

        self.absolute_size = max(1,3**(self.dimensions-3))
        self.extra_dimension_size = physics.extra_dimension_size
        if self.absolute_size > self.extra_dimension_size:
            self.extra_dimension_size = self.absolute_size + 0.1

        self.stars = [
            BackgroundStar(
                np.array((lambda r,theta,phi: [r**(1/self.dimensions)*np.sin(theta)*np.cos(phi), r**(1/self.dimensions)*np.sin(theta)*np.sin(phi), r**(1/self.dimensions)*np.cos(theta)])(*(random.uniform(self.box_size/10,self.box_size)*300**(self.dimensions-2),np.arccos(1-2*random.uniform(0,1)),random.uniform(0,2*np.pi))) + [random.uniform(0, self.extra_dimension_size) for _ in range(3,self.dimensions)]),
                self.absolute_size
                )
        for _ in range(self.number) ]

        self.stars = sorted(self.stars, key=lambda x: np.linalg.norm(x.position))

        for star in self.stars:
            star.compute_apparent_magnitude(self.dimensions, self.ldf)
            star.compute_projected_position(self.width, self.height, self.extra_dimension_size)

        self.k = np.array([random.uniform(-1,1) for _ in range(max(self.dimensions, 3))])
        self.k = self.k/np.linalg.norm(self.k)
        self.theta = 0.001
        if self.dimensions == 2:
            r = np.array([[np.cos(self.theta), np.sin(self.theta)],[-np.sin(self.theta), np.cos(self.theta)]])
        else:
            k = np.array([[0, -self.k[2], self.k[1]],[self.k[2], 0, -self.k[0]],[-self.k[1], self.k[0], 0]])
            r = np.eye(k.shape[0]) + np.sin(self.theta)*k + (1-np.cos(self.theta))*np.dot(k,k)
        s = r.shape[0]
        m = np.zeros((max(self.dimensions, 3),max(self.dimensions, 3)))
        
        i = np.eye(max(self.dimensions, 3)-s)
        m[:s,:s] = r
        m[s:,s:] = i
        self.rotation_matrix = m

    def update(self):
        for star in self.stars:
            self._rotate_star(star)
            star.compute_apparent_magnitude(self.dimensions, self.ldf)
            star.compute_projected_position(self.width, self.height, self.extra_dimension_size)
    
    def _rotate_star(self, star):
        star.position = np.dot(self.rotation_matrix, star.position)
        for i in range(3,len(star.position)):
            star.position[i] = (star.position[i] + 20*self.theta) % self.extra_dimension_size

class Physics():

    def __init__(self, dimensions=3, relativistic=True, dt=10.0, G=100.0, c=300.0, ldf=None, extra_dimension_size=None, torus=False, klein=False, size=(1280, 720)):
        # Spatial dimensions
        #  can also be non-integer, will influence gravitational force. The integer part
        #  matters for the background
        self.dimensions = dimensions
        # Relativity
        #  use relativistic computations if avaliable
        self.relativistic = relativistic
        # Time step
        self.dt = dt
        # Gravitational constant
        self.G = G
        # Speed of light
        #  matters for relativistic effects
        self.c = c
        # Light dissipation factor
        #  coefficient in calculating apparent magnitude of background stars
        if ldf is None:
            self.ldf = 10*(100)**(int(np.ceil(self.dimensions))-1)
        else:
            self.ldf = ldf
        # Size of hidden dimensions
        #  this is use for background physics only.
        if extra_dimension_size is None:
            self.extra_dimension_size = max(1,3**(self.dimensions-3)) + 0.1
        else:
            self.extra_dimension_size = extra_dimension_size

        self.size = size

        self.torus = torus
        self.klein = klein

    def _newton_any_d_gradv(self, pos, mass, radius, d=2):
        r0 = 0.01
        r_norm = np.linalg.norm(pos)
        r_hat = pos/r_norm
        if r_norm < radius:
            return self.G*mass/radius**(d-1)*r_hat*r0**(3-d)
        else:
            return self.G*mass/r_norm**(d-1)*r_hat*r0**(3-d)

    def _einstein_3D_gradv(self, pos, vel, mass):
        rs = 2*self.G*mass/self.c**2

        x, y = pos
        r = np.linalg.norm(pos)

        vel_x, vel_y = vel

        diff = (r-rs)

        if diff < 0:
            return np.array((0,0), dtype=np_float)

        acc_x = rs/(2*diff*r**5)*(
            self.c**2*diff**2*r*x \
            - x*(3*r*x**2 + 2*rs*y**2)*vel_x**2 \
            - 2*y*(2*(2*r-rs)*x**2+r*y**2)*vel_x*vel_y \
            - x*(r*y**2 - 2*diff*x**2)*vel_y**2
        )
        acc_y = rs/(2*diff*r**5)*(
            self.c**2*diff**2*r*y \
            - y*(3*r*y**2 + 2*rs*x**2)*vel_y**2 \
            - 2*x*(2*(2*r-rs)*y**2+r*x**2)*vel_x*vel_y \
            - y*(r*x**2 - 2*diff*y**2)*vel_x**2
        )

        return np.array((acc_x, acc_y), dtype=np_float)

    def gradv(self, pos, vel, mass, radius):
        if np.floor(self.dimensions) == 3 and self.relativistic:
            return self._einstein_3D_gradv(pos, vel, mass)
        else:
            return self._newton_any_d_gradv(pos, mass, radius, self.dimensions)

    def _integrand(self, _, y, sources):
        pos_x, pos_y, vel_x, vel_y = y
        pos = np.array((pos_x, pos_y), dtype=np_float)
        vel = np.array((vel_x, vel_y), dtype=np_float)
        gradv = np.array((0, 0), dtype=np_float)
        for source in sources:
            if self.torus:
                w,h = 1,2
                for m, n in product(range(-w,w+1), range(-h,h+1)):
                    gradv = gradv - self.gradv(pos - (source.pos + np.array([m*self.size[0], n*self.size[1]])), vel, source.mass, source.radius)
            elif self.klein:
                w,h = 1,2
                for m, n in product(range(-w,w+1), range(-h,h+1)):
                    if n % 2 == 0:
                        gradv = gradv - self.gradv(pos - (source.pos + np.array([m*self.size[0], n*self.size[1]])), vel, source.mass, source.radius)
                    else:
                        npos = np.array((-source.pos[0] % self.size[0], source.pos[1]), dtype=np_float)
                        gradv = gradv - self.gradv(pos - (npos + np.array([m*self.size[0], n*self.size[1]])), vel, source.mass, source.radius)
            else:
                gradv = gradv - self.gradv(pos - source.pos, vel, source.mass, source.radius)
        return (vel_x, vel_y, *gradv)

    def ode_solve(self, sources, probe):
        integrand = lambda t,y: self._integrand(t,y,sources)
        sol = solve_ivp(integrand, (0, self.dt), (*probe.pos, *probe.vel), atol=np_float('5e-2'), rtol=np_float('5e-2'))
        pos_x, pos_y, v_x, v_y = sol.y[:,-1]
        if np.linalg.norm(np.array([v_x, v_y])) > self.c or np.linalg.norm(probe.pos - np.array((pos_x, pos_y)))/(sol.t[-1] - sol.t[0]) > self.c:
            return np.array((*probe.pos, *(0,0)))
        return sol.y[:,-1]

class BoxedText():

    def __init__(self, title, text, title_font=None, text_font=None, colors=Colors(), background_color=None, font_color=None):
        self.title = title
        self.text = text
        if title_font is None:
            self.title_font = pygame.font.Font('freesansbold.ttf', 20)
        else:
            self.title_font = title_font
        if text_font is None:
            self.text_font = pygame.font.Font('freesansbold.ttf', 16)
        else:
            self.text_font = text_font

        self.colors = colors

        if background_color is None:
            self.background_color = self.colors.dark_gray
        else:
            self.background_color = background_color

        if font_color is None:
            self.font_color = self.colors.white
        else:
            self.font_color = font_color

    def draw(self, screen):

        if self.text == '':
            return None

        # Render text
        if self.title != '':
            title = self.title_font.render(self.title, True, self.font_color)
        else:
            title = None
        text_lines = self.text.split('\n')
        rendered_text = [ self.text_font.render(line, True, self.font_color) for line in text_lines ]

        # Draw the box
        if title is not None:
            width = title.get_width()
            height = title.get_height()
        else:
            width = 0
            height = 0
        for text in rendered_text:
            if text.get_width() > width:
                width = text.get_width()
            height += text.get_height()
        height_padding = 10
        width_padding = 10
        height += (len(rendered_text)+2)*height_padding

        left = screen.get_width()/2 - width/2
        top = screen.get_height()/2 - height/2

        rect = pygame.Rect(left-width_padding, top, width+2*width_padding, height)
        pygame.draw.rect(screen, self.colors.dark_gray, rect)

        # Draw the text
        height_diff = height_padding
        if title is not None:
            screen.blit(title, (screen.get_width()/2-title.get_width()/2, top+height_diff))
            height_diff += title.get_height() + height_padding
        for text in rendered_text:
            screen.blit(text, (screen.get_width()/2-text.get_width()/2, top+height_diff))
            height_diff += text.get_height() + height_padding

class Game():

    def __init__(self, physics=None, show_fps=True, boxed=False, collision=True, presentation=None, shots=np.inf, score=0.0):
        self.width = 1280
        self.height = 720

        self.screen = pygame.display.set_mode((self.width, self.height), pygame.SCALED, display=0)

        self.colors = Colors()

        self.font = pygame.font.Font('freesansbold.ttf', 16)
        self.title_font = pygame.font.Font('freesansbold.ttf', 20)

        self.FPS = 60
        self.clock = pygame.time.Clock()

        self.show_fps = show_fps
        self.fps_timer = time.time()
        self.fps_counter = 0
        self.fps_current = self.FPS

        if physics is None:
            self.physics = Physics()
        else:
            self.physics = physics

        self.boxed = boxed
        self.compact = any((self.boxed, self.physics.torus, self.physics.klein))

        self.reflection_coeff = 0.8

        self.collision = collision

        self.background = Background(self.width, self.height, self.physics)

        self.sources = []
        self.probes = []

        self.player_probe = None
        self.player_trace = []
        self.player_trace_length = 1000

        self.target = None
        self.won = False
        self.lost = False
        self.presentation = presentation

        self.mouse_click = None

        self.shots = shots

        self.old_score = score

        self.quit = False

    def draw(self):
        self.draw_background()
        if self.boxed:
            pygame.draw.rect(self.screen, self.colors.red, pygame.Rect(0, 0, self.width, self.height), 4)
        if self.physics.torus:
            pygame.draw.line(self.screen, self.colors.red, (0, int(self.height/2 - 50)), (0, int(self.height/2 + 50)), 5)
            pygame.draw.line(self.screen, self.colors.red, (0, int(self.height/2 + 50)), (10, int(self.height/2 + 50 - 10)), 5)

            pygame.draw.line(self.screen, self.colors.red, (self.width, int(self.height/2 - 50)), (self.width, int(self.height/2 + 50)), 5)
            pygame.draw.line(self.screen, self.colors.red, (self.width, int(self.height/2 + 50)), (self.width-10, int(self.height/2 + 50 -10)), 5)

            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 - 50), 0), (int(self.width/2 + 50), 0), 5)
            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 + 50), 0), (int(self.width/2 + 50 -10), 10), 5)

            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 - 50), self.height), (int(self.width/2 + 50), self.height), 5)
            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 + 50), self.height), (int(self.width/2 + 50 - 10), self.height -10), 5)
        if self.physics.klein:
            pygame.draw.line(self.screen, self.colors.red, (0, int(self.height/2 - 50)), (0, int(self.height/2 + 50)), 5)
            pygame.draw.line(self.screen, self.colors.red, (0, int(self.height/2 + 50)), (10, int(self.height/2 + 50 - 10)), 5)

            pygame.draw.line(self.screen, self.colors.red, (self.width, int(self.height/2 - 50)), (self.width, int(self.height/2 + 50)), 5)
            pygame.draw.line(self.screen, self.colors.red, (self.width, int(self.height/2 + 50)), (self.width-10, int(self.height/2 + 50 -10)), 5)

            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 - 50), 0), (int(self.width/2 + 50), 0), 5)
            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 - 50), 0), (int(self.width/2 - 50 + 10), 10), 5)

            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 - 50), self.height), (int(self.width/2 + 50), self.height), 5)
            pygame.draw.line(self.screen, self.colors.red, (int(self.width/2 + 50), self.height), (int(self.width/2 + 50 - 10), self.height -10), 5)
        for pos in self.player_trace:
            pygame.draw.circle(self.screen, self.colors.gray, np.array(pos.round(), dtype=int), 1)
        for source in self.sources:
            source.update()
            source.draw(self.screen)
        for probe in self.probes + ([self.player_probe] if self.player_probe is not None else []):
            probe.draw(self.screen)
        if self.target is not None:
            self.target.draw(self.screen)
        if not self.compact:
            if self.player_probe is not None:
                x,y = self.player_probe.pos
                if (not 0 < x < self.width) or (not 0 < y < self.height):
                    self.draw_escape_arrow()
        if self.show_fps:
            self.draw_fps()
        if self.player_probe is not None:
            self.draw_score()
        self.draw_presentation()
        if self.mouse_click is not None:
            mp = self.mouse_power()
            if mp is None:
                self.screen.blit(self.font.render('0', True, self.colors.green), pygame.mouse.get_pos())
            else:
                direction, n = mp
                direction = np.roll(direction, -1)*np.array((-1,1), dtype=np_float)
                self.screen.blit(self.font.render(str(round(n,1)), True, self.colors.green), self.mouse_click + 20*direction)
            pygame.draw.line(self.screen, self.colors.green, self.mouse_click, pygame.mouse.get_pos(), 5)
        self.draw_winning_text()
        self.draw_losing_text()
        #self.screen.blit(self.font.render(f'Vel: {round(np.linalg.norm(self.player_probe.vel,2))} ({round(np.linalg.norm(self.player_probe.vel)/self.physics.c,2)})', True, self.colors.red), (1180,690))
        pygame.display.update()

    def draw_background(self):
        self.screen.fill(self.background.background)
        for star in self.background.stars:
            if star.draw:
                pygame.draw.circle(self.screen, star.color, np.array(star.projected_position.round(),dtype=int), int(round(star.apparent_magnitude)))

    def draw_escape_arrow(self):
        # has left the screen, show an arrow
        m = np.array((self.width/2,self.height/2), dtype=np_float)
        r_hat = self.player_probe.pos - m
        r_hat = r_hat/np.linalg.norm(r_hat)
        u = 0
        if r_hat[0] == 0:
            u = self.height/2
        elif r_hat[1] == 0:
            u = self.width/2
        else:
            #TODO: This argument can be empty, causing a crash. Figure out why.
            try:
                u = min([ i for i in [((0-m[0])/r_hat[0]),((self.width-m[0])/r_hat[0]),((0-m[1])/r_hat[1]),((self.height-m[1])/r_hat[1])] if i > 0])
            except ValueError as e:
                print(f'm: {m}')
                print(f'r_hat: {r_hat}')
                print(f'u candidates: {[ i for i in [((0-m[0])/r_hat[0]),((self.width-m[0])/r_hat[0]),((0-m[1])/r_hat[1]),((self.height-m[1])/r_hat[1])]]}')
                raise e
        # intersection point with edge:
        l = m + u*r_hat
        pygame.draw.line(self.screen, self.colors.red, l, l - (lambda r: r**1.5/(r+1))(np.linalg.norm(self.player_probe.pos - m))*r_hat, 5)

    def draw_fps(self):
        if time.time() - self.fps_timer > 1:
            self.fps_current = self.fps_counter
            self.fps_counter = 0
            self.fps_timer = time.time()
        else:
            self.fps_counter += 1
        self.screen.blit(self.font.render(f'FPS: {self.fps_current} ({self.FPS})', True, self.colors.red), (10,10))

    def score(self):
        if self.player_probe is not None:
            return round(self.player_probe.distance/10,1)
        else:
            return 0

    def draw_score(self):
        text = f'Score: {self.score()}'
        if self.old_score > 0:
            text += f' ({self.old_score})'
        self.screen.blit(self.font.render(text, True, self.colors.white), (10,self.height-26))

    def draw_winning_text(self):
        if self.won and self.presentation is None:
            BoxedText(
                title = 'You won!',
                title_font = self.title_font,
                text = f'You reached the target and got a score of: {self.score()}.\nPress [SPACE] to exit the level, or "r" to retry.',
                text_font = self.font
            ).draw(self.screen)

    def draw_losing_text(self):
        if self.lost and self.presentation is None:
            BoxedText(
                title = 'Your probe crashed!',
                title_font = self.title_font,
                text = f'Press [SPACE] to exit the level, or "r" to retry.',
                text_font = self.font
            ).draw(self.screen)

    def draw_presentation(self):
        if self.presentation is not None:
            self.presentation.draw(self.screen)

    def update(self):
        self.background.update()
        if self.won or self.lost:
            return None
        for probe in self.probes + ([self.player_probe] if self.player_probe is not None else []):
            y = self.physics.ode_solve(self.sources, probe)
            probe.update(y)
            rel = np.linalg.norm(probe.vel)/self.physics.c
            if rel > 1:
                print(f'Rel: {rel}')
            if self.collision:
                for source in self.sources:
                    if self.presentation is None:
                        collided = probe.collide(source, bounce=True)
                        if collided == True:
                            self.lost = True
                    else:
                        collided = probe.collide(source, bounce=True)
            if self.boxed:
                self._box_probe(probe)
            if self.physics.torus:
                self._torify_probe(probe)
            if self.physics.klein:
                self._kleinify_probe(probe)
        if self.player_probe is not None:
            self.player_trace.append(self.player_probe.pos)
            if len(self.player_trace) > self.player_trace_length:
                self.player_trace = self.player_trace[1:]
            # win condition
            self.won = self.player_probe.collide(self.target, bounce=False)

    def _box_probe(self, probe):
        if probe.x - probe.radius < 0 and probe.vel[0] < 0:
            probe.set_velocity(probe.vel*np.array((-1*self.reflection_coeff,1), dtype=np_float))
        if probe.x + probe.radius > self.width and probe.vel[0] > 0:
            probe.set_velocity(probe.vel*np.array((-1*self.reflection_coeff,1), dtype=np_float))
        if probe.y - probe.radius < 0 and probe.vel[1] < 0:
            probe.set_velocity(probe.vel*np.array((1,-1*self.reflection_coeff), dtype=np_float))
        if probe.y + probe.radius > self.height and probe.vel[1] > 0:
            probe.set_velocity(probe.vel*np.array((1,-1*self.reflection_coeff), dtype=np_float))

    def _torify_probe(self, probe):
        if probe.x < 0 and probe.vel[0] < 0 or probe.x > self.width and probe.vel[0] > 0:
            probe.update(np.array((*(probe.x % self.width, probe.y), *probe.vel), dtype=np_float))
        if probe.y < 0 and probe.vel[1] < 0 or probe.y > self.height and probe.vel[1] > 0:
            probe.update(np.array([*(probe.x, probe.y % self.height), *probe.vel], dtype=np_float))

    def _kleinify_probe(self, probe):
        if probe.x < 0 and probe.vel[0] < 0 or probe.x > self.width and probe.vel[0] > 0:
            probe.update(np.array((*(probe.x % self.width, probe.y), *probe.vel), dtype=np_float))
        if probe.y < 0 and probe.vel[1] < 0 or probe.y > self.height and probe.vel[1] > 0:
            probe.update(np.array([*(-probe.x % self.width, probe.y % self.height), *(probe.vel * np.array((-1,1)))], dtype=np_float))

    def tick(self):
        self.clock.tick(self.FPS)

    def add_source(self, source):
        self.sources.append(source)

    def add_probe(self, probe):
        self.probes.append(probe)

    def add_player_probe(self, probe):
        self.player_probe = probe

    def add_target(self, target):
        self.target = target

    def mouse_down(self, pos):
        self.mouse_click = np.array(pos, dtype=np_float)

    def mouse_power(self, pos=None):
        if self.mouse_click is not None:
            if pos is None:
                pos = np.array(pygame.mouse.get_pos(), dtype=np_float)
            drag = self.mouse_click - np.array(pos, dtype=np_float)
            n = np.linalg.norm(drag)
            if n == 0:
                return None
            direction = drag/n
            #n = np.log(n+1)**2
            n = n/(n+self.physics.c**(1.2))*self.physics.c
            return direction, n
        else:
            return None

    def mouse_up(self, pos):
        if self.player_probe is None or self.shots == 0:
            self.mouse_click = None
            return None
        power = self.mouse_power(pos)
        self.mouse_click = None
        if power is None:
            return None
        direction, n = power
        self.player_probe.set_velocity(direction*n)
        self.shots -= 1

    def run(self):
        running = True

        retry = False

        while running:
            self.tick()

            self.draw()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    self.quit = True
                    running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        self.quit = True
                        running = False
                    if self.won or self.lost or self.presentation is not None:
                        if event.key == pygame.K_SPACE:
                            running = False
                    if event.key == pygame.K_r:
                        running = False
                        retry = True
                    if event.key == pygame.K_h:
                        self.presentation = BoxedText(title='', text='')
                    if event.key == pygame.K_x:
                        running = False

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouse_down(event.pos)

                if event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        self.mouse_up(event.pos)

            self.update()

        time.sleep(0.1)

        return retry

class Source():

    def __init__(self, position=(0,0), mass=None, radius=None, density=10*100/3375, physics=None, color=(0,128,0), u=0, dt=0, motion=None):

        self.pos = np.array(position, dtype=np_float)

        self.density = np_float(density)
        if mass is None and radius is None:
            self.mass = np_float(100)
            self.radius = (self.mass / self.density)**(1/3)
        elif mass is None and radius is not None:
            self.radius = np_float(radius)
            self.mass = self.density * self.radius**3
        elif mass is not None and radius is None:
            self.mass = np_float(mass)
            self.radius = (abs(self.mass) / self.density)**(1/3)
        else:
            self.mass = np_float(mass)
            self.radius = np_float(radius)

        if physics is None:
            physics = Physics()

        self.horizon = 0.0
        if physics.dimensions == 3 and physics.relativistic:
            if 2*self.mass*physics.G/physics.c**2 > self.radius:
                # has an event horizon
                self.horizon = 2*self.mass*physics.G/physics.c**2

        self.color = color

        self.u = u
        self.dt = dt
        self.motion = motion

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, np.array(self.pos.round(), dtype=int), int(round(self.radius)))
        if self.horizon > 0.0:
            pygame.draw.circle(screen, self.color, np.array(self.pos.round(), dtype=int), int(round(self.horizon)), 1)

    def update(self):
        if self.motion is None:
            return None
        self.u = self.u + self.dt
        self.pos = self.pos + self.motion(self.u)

class Probe():
    
    def __init__(self, position, velocity=(0,0), radius=5, color=(0,0,128), static=True):

        self.pos = np.array(position, dtype=np_float)
        self.x = self.pos[0]
        self.y = self.pos[1]
        self.pos_old = np.array(self.pos, dtype=np_float)
        self.vel = np.array(velocity, dtype=np_float)

        self.radius = radius
        self.color = color

        self.distance = 0.0

        self.static = static

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, np.array(self.pos.round(), dtype=int), int(round(self.radius)))

    def update(self, pos, vel=None):
        self.pos_old = self.pos
        if self.static:
            return None
        if vel is None:
            # mainly used after ode solution
            self.x, self.y, vel_x, vel_y = pos
            npos = np.array((self.x, self.y), dtype=np_float)
            n = np.linalg.norm(self.pos - npos)
            if n > 2.5:
                self.distance += n
            self.pos = npos
            self.vel = np.array((vel_x, vel_y), dtype=np_float)
        else:
            # mainly used at collisions
            n = np.linalg.norm(self.pos - pos)
            if n > 3:
                self.distance += n
            self.pos = np.array(pos, dtype=np_float)
            self.vel = np.array(vel, dtype=np_float)

    def set_velocity(self, vel):
        self.static = False
        self.vel = vel

    def collide(self, source, bounce=True):
        if source is None:
            return False

        radius_tot = self.radius + source.radius

        if np.linalg.norm(source.pos - self.pos) < radius_tot and np.linalg.norm(source.pos - self.pos_old) < radius_tot:
            if not bounce:
                return True
            # if has somehow got stuck in the source, move it outside.
            velocity = self.vel if np.linalg.norm(self.vel) > 0 else np.array((1,0), dtype=np_float)
            position = self.pos - velocity/np.linalg.norm(velocity)*(radius_tot - np.linalg.norm(source.pos - self.pos) + source.radius/100)
            velocity = np.array((0,0), dtype=np_float)
            self.update(position, velocity)
            return True

        path = self.pos - self.pos_old
        if np.linalg.norm(path) == 0:
            return False
        path_direction = path/np.linalg.norm(path)

        collision_position = (0,0)
        u_max = 1
        u = 0
        D = (np.dot(path_direction, self.pos_old - source.pos))**2 - (np.dot(self.pos_old - source.pos, self.pos_old - source.pos) - radius_tot**2)
        if D > 0:
            # potential collision happened
            u_a = -np.dot(path_direction, self.pos_old - source.pos)
            u_b = np.sqrt(D)

            u1 = u_a + u_b
            u2 = u_a - u_b

            u_max = np.dot(path, path_direction)
            if 0 < u1 < u_max and u1 < u2:
                u = u1
            elif 0 < u2 < u_max and u2 < u1:
                u = u2
            else:
                # no collision
                return False

            collision_position = self.pos_old + u * path_direction
        else:
            return False

        if not bounce:
            return True

        # to collision, probe travelled `u` distance. There is `u_max - u` left to travel,
        # which must happen after collision

        reflection_direction = (collision_position - source.pos)/np.linalg.norm(collision_position - source.pos)

        # new velocity
        velocity = -reflection_direction*self.vel*0.4
        # since we are colliding with sources, the acc can be quite high,
        # so we need high dampening of the velocity

        # new position
        position = collision_position + (u_max - u) * velocity/np.linalg.norm(velocity)*0.4
        # also with dampening

        if np.linalg.norm(source.pos - position) < radius_tot:
            # it did not manage to escape the source
            position = position - velocity/np.linalg.norm(velocity)*(radius_tot - np.linalg.norm(source.pos - position) + source.radius/100)
            velocity = np.array((0,0), dtype=np_float)

        self.update(position, velocity)
        return True

class Levels():

    def __init__(self):

        self.levels = [
            'welcome_screen',
            'goal_of_the_game_screen',
            'probe_and_target_screen',
            'sources_screen',
            'three_dim_newton_level',
            'boring_newton_screen',
            'relativity_screen',
            'three_dim_einstein_level',
            'two_dim_newton_screen',
            'two_dim_newton_level',
            'four_dim_newton_screen',
            'four_dim_newton_level',
            'twohalf_dim_newton_screen',
            'twohalf_dim_newton_level',
            'demo_end_screen',
            'moving_source_level',
            'random_sources_level',
            'blackhole_screen',
            'blackhole_level',
            'boxed_level',
            'torus_screen',
            'torus_level',
            'klein_bottle_level',
            'end_screen'
        ]

        self.scores = [ 0 for _ in range(len(self)) ]

    def __len__(self):
        return len(self.levels)

    def __getitem__(self, i):
        return eval(f'self.{self.levels[i]}()')

    def welcome_screen(self):
        bt = BoxedText(
            title='Welcome to Gravity-game!',
            text='Press [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)

        return game

    def goal_of_the_game_screen(self):
        bt = BoxedText(
            title='What is this game?',
            text='The goal of this game is to get your probe into a target.\nYou steer the probe by giving it an initial velocity by clicking and draging the mouse.\nYou can try on the background already.\n Press [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)

        return game

    def probe_and_target_screen(self):
        bt = BoxedText(
            title='',
            text='The probe is shown as the blue circle (above this box).\nThe target is a yellow circle (below this box).\n\nTry giving the probe a velocity with the mouse\n\nA red line will show where the probe is if it leaves the screen.\nTo get the probe back, try shooting it back, or reset the level by pressing "r".\nIf you hit the target, the game will stop.\nThe distance travelled by your probe is your score, which you can see in the lower left corner.\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2,game.height/2-200), color=game.colors.blue))
        game.add_target(Source(position=(game.width/2,game.height/2+200), mass=0, radius=30, color=game.colors.yellow))

        return game

    def sources_screen(self):
        bt = BoxedText(
            title='',
            text='The probe and target by themselves are not too fun to play with.\nFor this purpose we have sources of different masses and radii.\nBelow you see the probe having an elliptic orbit around a massive source.\n\nYou can change the velocity of the probe and reset if you like.\nYou can also hide this text box by pressing "h"; remember restart with "r" or go to next level with [SPACE]\n\nNext level will be the first actual level, and you will only get one shot with the probe.'
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2+200), velocity=np.array((0,-5)), color=game.colors.blue, static=False))
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=200, color=game.colors.green))

        return game

    def three_dim_newton_level(self):
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width/2,game.height/2), mass=200, color=game.colors.green))
        game.add_target(Source(position=(game.width/2+300,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def boring_newton_screen(self):
        bt = BoxedText(
            title='',
            text='The physics simulated here is Newtonian and three-dimensional.\nYou can also tell by the background which is also a 3D simulation.\nThe boring thing in Newtonian 3D is that the orbits are rather uninteresting:\nEllipses, Circles, and escaping paths.\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2+200), velocity=np.array((0,-5)), color=game.colors.blue, static=False))
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=200, color=game.colors.green))

        return game

    def relativity_screen(self):
        bt = BoxedText(
            title='',
            text='Now we have changed to relativistic physics in the simulation.\nYou see this in how the ellipse has a precession.\n\nYou can use this to get orbits that lake longer paths to get to the target.\n\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=3, relativistic=True, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2+200), velocity=np.array((0,-5)), color=game.colors.blue, static=False))
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=200, color=game.colors.green))

        return game

    def three_dim_einstein_level(self):
        physics = Physics(dimensions=3, relativistic=True, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width/2,game.height/2), mass=200, color=game.colors.green))
        game.add_target(Source(position=(game.width/2+300,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def two_dim_newton_screen(self):
        bt = BoxedText(
            title='',
            text='This game can also simulate physics in other dimension.\nPhysics is now 2D, and Newtonian again.\nThe background have also changed (carousel rotating star-pattern) to indicate this.\nOrbits are even more interesting now.\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=2, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2+200), velocity=np.array((0,-5)), color=game.colors.blue, static=False))
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=200, color=game.colors.green))

        return game

    def two_dim_newton_level(self):
        physics = Physics(dimensions=2, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width/2,game.height/2), mass=200, color=game.colors.green))
        game.add_target(Source(position=(game.width/2+300,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def four_dim_newton_screen(self):
        bt = BoxedText(
            title='',
            text='We can even have large extra dimensions. This is 4 dimensions.\nIt is not very fun though, since there are no stable orbits.\nThe probe will very easily leave, or crash into sources.\nYou also see the background stars rotate ("blink") in and out of the extra dimensions.\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=4, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2+200), velocity=np.array((0,-4)), color=game.colors.blue, static=False))
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=200, color=game.colors.green))

        return game

    def four_dim_newton_level(self):
        physics = Physics(dimensions=4, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width/2,game.height/2), mass=200, color=game.colors.green))
        game.add_target(Source(position=(game.width/2+300,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def twohalf_dim_newton_screen(self):
        bt = BoxedText(
            title='',
            text='We we can set any number of dimensions.\nThis is Newtonian physics in 2.5 dimensions.\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=2.5, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2+200), velocity=np.array((0,-4)), color=game.colors.blue, static=False))
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=200, color=game.colors.green))

        return game

    def twohalf_dim_newton_level(self):
        physics = Physics(dimensions=2.5, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)
        game.add_player_probe(Probe(position=(game.width/2-200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width/2,game.height/2), mass=200, color=game.colors.green))
        game.add_target(Source(position=(game.width/2+300,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def demo_end_screen(self):
        bt = BoxedText(
            title='End of demo',
            text='This is just a demo.\n\nNow you will be given some levels to play with different sources.'
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)

        return game

    def moving_source_level(self):
        physics = Physics(dimensions=2, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)

        game.add_source(Source(position=(game.width/2,game.height/2), mass=200, color=game.colors.green, dt=0.01, motion=lambda u: np.array([np.cos(u), np.sin(u)])))
        game.add_player_probe(Probe(position=(game.width/2+400,game.height/2), color=game.colors.blue))
        game.add_target(Source(position=(game.width/2-400,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def random_sources_level(self):
        physics = Physics(dimensions=2, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, shots=1)

        for _ in range(3):
            game.add_source(Source(position=(random.uniform(100,1180),random.uniform(50,680)), mass=200, color=game.colors.green))
        game.add_player_probe(Probe(position=(game.width/2+400,game.height/2), color=game.colors.blue))
        game.add_target(Source(position=(game.width/2-400,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def blackhole_screen(self):
        bt = BoxedText(
            title='Blackholes',
            text='The source you see below is a black hole. The ring around it represents the horizon.'
        )
        physics = Physics(dimensions=3, relativistic=True, dt=0.1)
        game = Game(physics=physics, boxed=False, shots=1, presentation=bt)
        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=10000, radius=3, color=game.colors.green))

        return game

    def blackhole_level(self):
        physics = Physics(dimensions=3, relativistic=True, dt=0.1)
        game = Game(physics=physics, boxed=False, shots=1)

        game.add_source(Source(position=(game.width/2,game.height/2+200), mass=10000, radius=3, color=game.colors.green))
        game.add_player_probe(Probe(position=(game.width/2+400,game.height/2), color=game.colors.blue))
        game.add_target(Source(position=(game.width/2-400,game.height/2), mass=0, radius=30, color=game.colors.yellow))

        return game

    def boxed_level(self):
        physics = Physics(dimensions=2, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=True, shots=1)

        game.add_source(Source(position=(game.width/2,game.height/2), mass=-200, color=game.colors.red))
        game.add_player_probe(Probe(position=(game.width/2+400,game.height/2), color=game.colors.blue))
        game.add_target(Source(position=(game.width/2-300,game.height/2), mass=0, radius=30, color=game.colors.yellow))
        game.add_source(Source(position=(game.width/2-300,game.height/2+100), mass=200, color=game.colors.green))
        game.add_source(Source(position=(game.width/2-300,game.height/2-100), mass=200, color=game.colors.green))
        game.add_source(Source(position=(game.width/2-300-100,game.height/2), mass=200, color=game.colors.green))

        return game

    def torus_screen(self):
        bt = BoxedText(
            title='',
            text='Notice the arrows at the edges of the screen?\nThese represent identification of opposite sides, and the arrows the orientation of that identification.\nThis way of identification corresponds to a Torus (the surface of a dougnut).\nUp-down and right-left are identified as if those directions were a circle.\nTry it out with the probe.\nPress [SPACE] to continue to the next screen'
        )
        physics = Physics(dimensions=4, relativistic=False, dt=0.5, torus=True)
        game = Game(physics=physics, shots=np.inf, presentation=bt)

        game.add_player_probe(Probe(position=(game.width/2,game.height/2+200), velocity=np.array((0,0)), color=game.colors.blue, static=False))

        return game

    def torus_level(self):
        physics = Physics(dimensions=4, relativistic=False, dt=0.5, torus=True)
        game = Game(physics=physics, shots=np.inf)

        game.add_player_probe(Probe(position=(game.width/2+200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width/2,game.height/2), mass=300, color=game.colors.green))
        game.add_source(Source(position=130*np.array([np.cos(45*np.pi/180),np.sin(45*np.pi/180)],dtype=np_float), mass=-100, color=game.colors.red))
        game.add_source(Source(position=130*np.array([np.cos(70*np.pi/180),np.sin(70*np.pi/180)],dtype=np_float), mass=-100, color=game.colors.red))
        game.add_source(Source(position=130*np.array([np.cos(20*np.pi/180),np.sin(20*np.pi/180)],dtype=np_float), mass=-100, color=game.colors.red))
        game.add_target(Source(position=(50,50), mass=0, radius=30, color=game.colors.yellow))

        return game

    def klein_bottle_level(self):
        physics = Physics(dimensions=3, relativistic=False, dt=0.5, klein=True)
        game = Game(physics=physics, shots=np.inf)

        game.add_player_probe(Probe(position=(game.width/2+200,game.height/2), color=game.colors.blue))
        game.add_source(Source(position=(game.width-200,game.height-200), mass=100, color=game.colors.green))
        game.add_target(Source(position=(50,50), mass=0, radius=30, color=game.colors.yellow))

        return game

    def end_screen(self):
        text = [ f'{self.levels[i]}: {self.scores[i]}' for i in range(len(self.levels)) if self.scores[i] > 0]
        text = '\n'.join(text)
        bt = BoxedText(
            title='Final score',
            text=text
        )
        physics = Physics(dimensions=3, relativistic=False, dt=0.5)
        game = Game(physics=physics, boxed=False, presentation=bt)

        return game

def main():

    pygame.font.init()

    levels = Levels()

    level_pointer = 0

    while True:
        game = levels[level_pointer]
        game.old_score = levels.scores[level_pointer]
        retry = game.run()
        score = game.score()
        if score > levels.scores[level_pointer] and game.won:
            levels.scores[level_pointer] = game.score()
        if not retry:
            level_pointer += 1
        if level_pointer >= len(levels) or game.quit:
            break

    print('Game finished running. Exiting.')
    pygame.quit()


if __name__ == '__main__':
    main()
