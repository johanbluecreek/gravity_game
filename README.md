# Gravity Game

Gravity game is a gravity simulation game in which you fling away a probe and by travelling the fields of gravitating bodies end up at a goal. You'll get to experience gravity in lower and higher dimensions than you are used to, negative masses, relativity, and black holes.

## Install

To install you can
```
$ git clone https://gitlab.com/johanbluecreek/gravity_game.git
$ cd gravity_game/
$ python3 -m venv .venv
$ source .venv/bin/activate
$ python3 -m pip install -r requirements.txt
$ deactivate
```
or take a look at `requirements.txt` and install those dependencies anyway you like.

## Running the game

```
$ ./gravity.py
```

and follow the prompts (if you are using a virtual environment, first run `source .venv/bin/activate`, and `deactivate` when you are done).

## Basic controls

* Click, drag & release - Flinging the probe
* `[Space]` - continue to next level/skip presentation prompt
* `r` - restart level to reset probe or try for better score
* `[Escape]` - close the game
* `x` - debug key for skipping level
